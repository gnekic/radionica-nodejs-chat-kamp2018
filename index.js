const dotenvResult = require('dotenv').config();

const express = require('express');
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const badWords = require('./lib/badWords.js');
const randomColors = require('./lib/randomColors.js');

function objaviSveKorisnike() {
    io.emit('users', korisnici);
}

function findByIdKorisnik(id) {
    for (let i = 0; i < korisnici.length; i++) {
        if (id === korisnici[i].id) { return korisnici[i]; }
    }
    return undefined;
}
function findById(id) {
    for (let i = 0; i < korisnici.length; i++) {
        if (id === korisnici[i].id) { return i; }
    }
    return -1;
}

let idGlobalCount = 0;
let korisnici = [];

io.on('connection', function(socket){
    socket.emit('clear')
    socket.emit('chat message', 'Server started...')
    // Oznaci korisnika sa ID
    const idSocketa = idGlobalCount;
    socket.join('user_'+idSocketa);

    korisnici.push({
        id: idSocketa,
        username: 'Anonymous_'+idSocketa,
        color: randomColors().hex(),
        isAdmin: false,
        isApproved: false,
    });
    console.log('New user connected! ID:', idSocketa);
    console.log(korisnici);
    objaviSveKorisnike();
    
    idGlobalCount++;

    let poruke = [];
    let zadnjeVrijemePoslanePoruke = 0;

    // Auth
    let username = 'Anonymous';
    let isUserAdmin = false;

/*  
    socket.on('login', (username, password) => {
        if (username === 'admin' && password === '1234') {
            isUserAdmin = true;
            return;
        }
        isUserAdmin = false;
    });
       
    idGlobalCount++;

    // Auth
    let username = 'Anonymous';
    let isUserAdmin = false;

/*  
    socket.on('login', (username, password) => {
        if (username === 'admin' && password === '1234') {
            isUserAdmin = true;
            return;
        }
        isUserAdmin = false;
    });
*/
    
    socket.on('chat message', function(str){
        if (typeof str !== 'string') { return; }
        if (str.length > 256) { return; }

        const command = str.match(/^\/(.+)$/)
        if (command) {
            console.log(str);
            const loginRegex = command[1].match(/^login\s(.+?)\s(.+)$/);
            if (loginRegex) {
                const username = loginRegex[1];
                const password = loginRegex[2];
                if (username === 'admin' && password === process.env.ADMIN_PASSWORD) {
                    isUserAdmin = true;
                    socket.emit('chat message', 'Dobrodošao admine!');
                    return;
                }
                isUserAdmin = false;
                socket.emit('chat message', 'Access denied!!!');
                return;
            }
            const promjenaUsernameaRegex = command[1].match(/^nick\s(.+)$/);
            if (promjenaUsernameaRegex) {
                const noviNick = promjenaUsernameaRegex[1];
                if (!(noviNick.match(/^[a-zA-Z0-9]{1,13}$/))) {
                    socket.emit('chat message', 'Ne može taj nick!');
                    return;
                }
                for (let i = 0; i < korisnici.length; i++) {
                    if (korisnici[i].username.toUpperCase() === noviNick.toUpperCase()) {
                        socket.emit('chat message', 'Ne može taj nick, netko ga već koristi!');
                        return;
                    }
                }
                korisnici[findById(idSocketa)].username = noviNick;
                korisnici[findById(idSocketa)].isApproved = true;
                objaviSveKorisnike()
                return;
            }
            const privatnaPorukaRegex = command[1].match(/^pm\s(.+?)\s(.+)$/);
            if (privatnaPorukaRegex) {
                const idKorisnika = privatnaPorukaRegex[1];
                const porukaZaKorisnika = privatnaPorukaRegex[2]
                if (badWords.includes(porukaZaKorisnika)) { socket.emit('chat message', 'cccc...'); return; }
                io.to('user_'+idKorisnika).emit('chat message', ' <- ' + idSocketa + ' : ' + porukaZaKorisnika);
                socket.emit('chat message', ' -> ' + idKorisnika + ' : ' + porukaZaKorisnika);
                return;
            }
            if (command[1] === 'me') {
                socket.emit('chat message', 'Ti si ID: '+idSocketa);
                if (isUserAdmin) {
                    socket.emit('chat message', 'Ti si admin!');
                } else {
                    socket.emit('chat message', 'Ti NISI admin!');
                }
                return;
            }
            if (isUserAdmin) {
                if (command[1] === 'clear') {
                    io.emit('clear');
                    return;
                }
            }
            socket.emit('chat message', 'Nepoznata komanda!')
            return;
        }

        if (badWords.includes(str)) { socket.emit('chat message', 'cccc...'); return; }
        if (str === ' ' || str === '') { return; }
        // console.log('Nova poruka: ', str);
        if (korisnici[findById(idSocketa)].isApproved) {
            if ((Date.now() - zadnjeVrijemePoslanePoruke) < 400) {
                socket.emit('chat message', 'Pre brzo pišeš!')
                return;
            }

            io.emit('chat message', str, findByIdKorisnik(idSocketa))
            poruke.push(str);
            if (poruke.length > 5) { poruke.shift() }
            zadnjeVrijemePoslanePoruke = Date.now();
            console.log(poruke);
        } else {
            socket.emit('chat message', 'Moraš promijenit nick da bi pisao')
        }  
    });

    socket.on('disconnect', function(){
        console.log('User disconnected! ID: ' + idSocketa);
        for (let i = 0; i < korisnici.length; i++) {
            if (korisnici[i].id === idSocketa) {
                korisnici.splice(i, 1);
            }
        }
        console.log(korisnici);
        objaviSveKorisnike()
    });
});

setInterval(() => {
    
}, 1000);

app.use(express.static('public'))

// app.get('/test', (req, res) => {
//     res.send(`test`)
// });

const serverPort = 3005;
http.listen(serverPort, () => {
    console.log(`Server radi na portu ${serverPort}!`)
})

console.log('Moj chat server!');